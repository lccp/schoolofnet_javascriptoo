/**
 * Created by lcdaponte on 14/10/16.
 */


var value = {
    val: 10,

    getValue: function () {
      return this.val + 2;
    }

};


console.log(value.getValue());

var value2 = Object.create(value);

value2.getValue = function () {
  return this.val + 10;
};

console.log(value2.getValue());

console.log(value, value2);